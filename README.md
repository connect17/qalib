# QALib
## QA Library for backend

### Installation

In your project's composer.json, add these lines:

    require-dev": {
        "connect17/qalib": "dev-master",
        ...
    },
    "repositories": [
        ...,
        {
            "url": "git@bitbucket.org:connect17/qalib.git",
            "type": "git"
        },
        ...
    ]

### License

[BundleLaundry](http://www.bundlelaundry.com/) All Rights Reserved @2017
